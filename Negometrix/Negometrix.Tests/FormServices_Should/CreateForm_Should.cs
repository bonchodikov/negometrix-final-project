﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Negometrix.Data;
using Negometrix.Data.Entities;
using Negometrix.Services.DTOMappers;
using Negometrix.Services.DTOs;
using Negometrix.Services.Exceptions;
using Negometrix.Services.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Negometrix.Tests.FormServices_Should
{
    [TestClass]
    public class CreateForm_Should : BaseTest
    {
        [TestMethod]
        public async Task CreateForm_WithCorrectData()
        {
            var options = Utility.GetOptions(nameof(CreateForm_WithCorrectData));

            using (var contextConfig = new NegometrixContext(options))
            {


                var user = new User
                {
                    UserName = "testUser",
                    Id = 1,
                };

                contextConfig.Users.Add(user);

                await contextConfig.SaveChangesAsync();
                var questionType = new QuestionType
                {
                    QuestionTypeId = 1,
                    QuestionTypeName = "text"
                };

                var question = new Question()
                {
                    QuestionId = 1,
                    QuestionType = questionType
                };

                var listQ = new List<Question>();
                listQ.Add(question);

                var form = new Form
                {
                    FormId = Guid.NewGuid(),
                    FormDescription = null,
                    IsDeleted = false,
                    Questions = listQ
                };

                using (var assertContext = new NegometrixContext(options))
                {
                    var expectedQuestion = new Question()
                    {
                        QuestionId = 1,
                        QuestionType = questionType
                    };

                    var myProfile = new DTOAutoMapper();
                    var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
                    var mapper = new Mapper(configuration);
                    var expectedList = new List<QuestionDTO>();
                    expectedList.Add(mapper.Map<QuestionDTO>(question));

                    var expected = new FormDTO()
                    {
                        FormId = Guid.NewGuid(),
                        UserId = 1,
                        FormDescription = null,
                        Questions = expectedList
                    };

                    var sut = new FormService(mapper, contextConfig, dateTimeProvider);
                    var actual = await sut.CreateForm(expected);

                    Assert.AreEqual(expected.FormDescription, actual.FormDescription);
                    Assert.AreEqual(expected.FormId, actual.FormId);
                    Assert.AreEqual(expected.UserId, actual.UserId);
                    Assert.AreEqual(expected.Questions.Count, actual.Questions.Count);
                }
            }
        }
        [TestMethod]
        [ExpectedException(typeof(ResourceNotExistException),
  "There is no user with id: 0")]

        public async Task ShouldNotCreateForm_WithMissingParamsCorrectUserData()
        {
            var options = Utility.GetOptions(nameof(ShouldNotCreateForm_WithMissingParamsCorrectUserData));

            using (var contextConfig = new NegometrixContext(options))
            {


                var questionType = new QuestionType
                {
                    QuestionTypeId = 1,
                    QuestionTypeName = "text"
                };

                var question = new Question()
                {
                    QuestionId = 1,
                    QuestionType = questionType
                };

                var listQ = new List<Question>();
                listQ.Add(question);

                var form = new Form
                {
                    FormId = Guid.NewGuid(),
                    FormDescription = null,
                    IsDeleted = false,
                    Questions = listQ
                };
                using (var assertContext = new NegometrixContext(options))
                {
                    var expectedQuestion = new Question()
                    {
                        QuestionId = 1,
                        QuestionType = questionType
                    };

                    var myProfile = new DTOAutoMapper();
                    var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
                    var mapper = new Mapper(configuration);
                    var expectedList = new List<QuestionDTO>();
                    expectedList.Add(mapper.Map<QuestionDTO>(question));

                    var expected = new FormDTO()
                    {
                        FormId = form.FormId,
                        FormDescription = null,
                        Questions = expectedList
                    };

                    var sut = new FormService(mapper, contextConfig, dateTimeProvider);
                    var actual = await sut.CreateForm(expected);
                }
            }
        }


    }
}
