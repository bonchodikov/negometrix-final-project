﻿using Negometrix.Web.Models.Questions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Negometrix.Web.Models
{
    public class CreateFormViewModel
    {
        public CreateFormViewModel()
        {
           this.Questions = new List<CreateQuestionViewModel>();
        }

        [Required]
        [MinLength(8)]
        public string FormName { get; set; }
        public string FormDescription { get; set; }
        
        public List<CreateQuestionViewModel> Questions { get; set; }
    }
}
