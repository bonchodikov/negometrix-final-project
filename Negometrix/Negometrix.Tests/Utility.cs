﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Negometrix.Data;

namespace Negometrix.Tests
{
    [TestClass]
   public class Utility
    {
        [TestMethod]
        public static DbContextOptions<NegometrixContext> GetOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<NegometrixContext>().
                UseInMemoryDatabase(databaseName)
                .Options;
        }
    }
}
