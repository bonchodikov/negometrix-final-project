﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Negometrix.Services.Exceptions
{
    public class ResourceNotExistException : Exception
    {
        public ResourceNotExistException(string message) : base(message) { }
    }
}
