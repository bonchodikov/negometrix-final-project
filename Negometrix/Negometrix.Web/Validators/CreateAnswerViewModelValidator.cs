﻿using FluentValidation;
using Negometrix.Web.Models.Anwers;

namespace Negometrix.Web.Validators
{
    public class CreateAnswerViewModelValidator: AbstractValidator<CreateAnswerViewModel>
    {
        public CreateAnswerViewModelValidator()
        {
            RuleFor(x => x.TextResult).MaximumLength(150)
               .When(x => x.Question.QuestionType.QuestionTypeName.Equals("text") && x.Question.IsRequired.Equals(false) && x.Question.IsLong.Equals(true));

            RuleFor(x => x.TextResult).MaximumLength(50)
               .When(x => x.Question.QuestionType.QuestionTypeName.Equals("text") && x.Question.IsRequired.Equals(false) && x.Question.IsLong.Equals(false));

            RuleFor(x => x.TextResult).NotEmpty().MaximumLength(150)
                .When(x => x.Question.QuestionType.QuestionTypeName.Equals("text") && x.Question.IsRequired.Equals(true) && x.Question.IsLong.Equals(true));

            RuleFor(x => x.TextResult).NotEmpty().MaximumLength(50)
                .When(x => x.Question.QuestionType.QuestionTypeName.Equals("text") && x.Question.IsRequired.Equals(true) && x.Question.IsLong.Equals(false));

            //RuleFor(x => x.OptionsResult).Must(list => list.Count > 0)
            //    .When(x => x.Question.IsRequired.Equals(true) && x.Question.QuestionType.QuestionTypeName.Equals("option")).WithMessage("Please choose at least one option!");

            RuleFor(x => x.Files).Must(list => list!=null)
                .When(x => x.Question.IsRequired.Equals(true) && x.Question.QuestionType.QuestionTypeName.Equals("document")).WithMessage($"Please upload a file!");

        }
    }
}
