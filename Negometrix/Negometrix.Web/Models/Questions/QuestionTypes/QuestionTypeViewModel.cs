﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Negometrix.Web.Models.Questions.QuestionTypes
{
    public class QuestionTypeViewModel
    {
        public int QuestionTypeId { get; set; }
        public string QuestionTypeName { get; set; }
    }
}
