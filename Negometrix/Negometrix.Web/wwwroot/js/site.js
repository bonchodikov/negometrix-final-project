﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

$(document).ready(function () {

    'use strict';

    var c, currentScrollTop = 0,
        navbar = $('nav');

    $(window).scroll(function () {
        var a = $(window).scrollTop();
        var b = navbar.height();

        currentScrollTop = a;

        if (c < currentScrollTop && a > b + b) {
            navbar.addClass("scrollUp");
        } else if (c > currentScrollTop && !(a <= b)) {
            navbar.removeClass("scrollUp");
        }
        c = currentScrollTop;
    });


    //$('#formsTab').hover(function () {
    //    $(this).removeClass('fa-question-circle');
    //    $(this).addClass('fa-foursquare');
    //}, function () {
    //    $(this).removeClass('fa-foursquare');
    //    $(this).addClass('fa-question-circle');
    //});

    $('#formsTab').mouseenter(function () {
        $(this).removeClass('fa-question-circle');
        $(this).addClass('fa-foursquare');
    }).mouseleave(function () {
        $(this).removeClass('fa-foursquare');
        $(this).addClass('fa-question-circle');
    });

});

function onHoverTab() {
    $('#formsTab').mouseenter(function () {
        $(this).removeClass('fa-question-circle');
        $(this).addClass('fa-foursquare');
    }).mouseleave(function () {
        $(this).removeClass('fa-foursquare');
        $(this).addClass('fa-question-circle');
    });
}


function cancelFunction() {
    console.log(123);
    let modal = document.getElementById('id01');
    modal.style.display = 'none';

}
let counter = 0;
function myFunction() {
    let newdiv = document.createElement('div');
    newdiv.innerHTML = `<br><input style="color:green" placeholder='Enter email here' type='text'id=${counter} name='myInputs[]'> <span style="color:red"id="span${counter}"></span>`;
    counter += 1;
    document.getElementById("12").appendChild(newdiv);
}
function Send(formId) {
    let arr = [];
    let isTrue = true;
    for (let i = 0; i < counter; i++) {

        let input = document.getElementById(i);
        if (input.value) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (!re.test(String(input.value).toLowerCase())) {
                isTrue = false;
                $(`#span${i}`).text("Wrong Email!");
            }
            else {
                arr.push(input.value);
            }
        }
    }
    let formIdd = formId;
    if (isTrue) {

         //$.post('/AnswerForm/Share', $.param({ data: arr, formId: formId }, true), function (data, formId) {
         //   window.setTimeout(function () {
         //       window.location.href = '/AnswerForm/Index/' + formIdd;
         //   }, 1);
         //   $.get("Index");
         //});

        $.ajax({
            async: true,
            data: $.param({ data: arr, formId: formId }, true),
            type: "POST",
            url: '/AnswerForm/Share',
            success:
                function (Ivaka) {
                    window.setTimeout(function () {
                        window.location.href = '/AnswerForm/Index/' + formIdd;
                    }, 1);
                }
        });
    }
}

function GetSellectedValue() {
    $('#choseType').on('change', function () {
        var value = $(this).val();
        console.log(value);
    });
}
function createNode() {
    let node = document.createElement('div');
    node.className += "row";
    node.className += " product-list";
    return node;
}

let modal = document.getElementById('id01'),
    cancel = document.getElementById('cancelBtn');


function manipulateDataBasedOnSellectedOption(list, partialIdString) {


    var selectedType = list.options[list.selectedIndex];

    if (selectedType.value === 'none') {
        $('#' + partialIdString).children().hide();
        $('#RemoveQuestionBtn').show();
    }
    else {

        $('#' + partialIdString).children().hide();
        $('#' + selectedType.value + '-' + partialIdString).show();
        $('#RemoveQuestionBtn').show();
    }
}

let counterOpt = 0;
let countHolder = {};
function addOption(partialId) {

    let childIndex = $("#orderItemsContainer").children("#row-" + partialId).index()

    let property = childIndex + ""
    countHolder[property] = 0

    $("#orderItemsContainer").children(".card").eq(childIndex).find(".optionControll").children().each(function () {
        if ($(this).prop("tagName") !== "UL") {
            countHolder[property] += 1
        } else {
            $(this).first().children().each(function () {
                countHolder[property] += 1
            })
        }
    })
    counterOpt = countHolder[property]

    console.log(partialId, $("#orderItemsContainer").children(".card").eq(childIndex), property)

    let appendToId = "addOptionHere" + "-" + partialId;
    let currentQuestionId = 0;
    $("#orderItemsContainer").children().each(function (i) {
        let current = $(this).attr('id');

        if (current.indexOf(partialId) !== -1) {
            currentQuestionId = i;
        }
    });
    let currentId = partialId + "-divRemoveOpt" + counterOpt;
    if (true) {

    }
    let inputElement = `<div id="${currentId}"> <input name="Questions[${currentQuestionId}].Options[${counterOpt}]" placeholder="Option:${counterOpt + 1}"  class="btn btn-light btn-block" $ id ="partialId-option${counterOpt}"/> <br />
</div>`

     

    let div = document.getElementById(appendToId);
    $(div).append(inputElement);
}

function removeOption(value, partialId) {

    let questionIndex = $("#orderItemsContainer").children("#row-" + partialId).index();
    let optionIndex = value;
    let currentOptId = "optDel-" + partialId;
    $('#' + currentOptId).val(questionIndex + "." + optionIndex);

    $.ajax({
        async: false,
        data: $('#form').serialize(),
        type: "POST",
        url: '/Form/RemoveQuestionOption',
        success:
            function (partialView) {
                $('#orderItemsContainer').html(partialView);
                showMenu();
            }
    });

}

let questionCount = 0;



function removeQuestion(partialId) {

    let currentId = "isDel-" + partialId;
    $('#' + currentId).prop('checked', true);

    $.ajax({
        async: false,
        data: $('#form').serialize(),
        type: "POST",
        url: '/Form/RemoveQuestion',
        success:
            function (partialView) {
                $('#orderItemsContainer').html(partialView);
            }
    });
}

function showMenu() {
    $('#orderItemsContainer').find('.chosen-option').each(function () {

        let partialIdString = ($(this).attr("data-optionid"));
        let selectedType = $(this).val();

        if (selectedType === 'none') {
            $('#' + partialIdString).children().hide();
            $('#RemoveQuestionBtn').show();
        }
        else {
            $('#' + partialIdString).children().hide();
            $('#' + selectedType + '-' + partialIdString).show();
            $('#RemoveQuestionBtn').show();
        }
    });
}
