﻿using AutoMapper;
using Negometrix.Services.DTOs;
using Negometrix.Web.Models;
using Negometrix.Web.Models.AnswerForms;
using Negometrix.Web.Models.Anwers;
using Negometrix.Web.Models.Questions;
using Negometrix.Web.Models.Questions.QuestionTypes;
using System.Security.Cryptography;

namespace Negometrix.Web.ModelMappers
{
    public class ModelAutoMapper : Profile
    {
        public ModelAutoMapper()
        {
            CreateMap<FormDTO, ListFormViewModel>().ReverseMap();

            CreateMap<FormDTO, FormViewModel>().ReverseMap();

            CreateMap<FormDTO, CreateFormViewModel>().ReverseMap();

            CreateMap<FormViewModel, CreateFormViewModel>().ReverseMap();

            CreateMap<QuestionTypeDTO, QuestionTypeViewModel>().ReverseMap();

            CreateMap<QuestionDTO, CreateQuestionViewModel>().ReverseMap();

            CreateMap<FormDTO, CreateFormViewModel>().ReverseMap();

            CreateMap<AnswerFormDTO, CreateAnswerFormViewModel>().ReverseMap();

            CreateMap<AnswerFormDTO, ListAnswerFormViewModel>().ReverseMap();

            CreateMap<AnswerFormDTO, AnswerFormViewModel>().ReverseMap();

            CreateMap<AnswerDTO, CreateAnswerViewModel>().ReverseMap();
        }
    }
}
