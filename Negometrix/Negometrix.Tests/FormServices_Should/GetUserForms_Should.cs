﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Negometrix.Data;
using Negometrix.Data.Entities;
using Negometrix.Services.DTOMappers;
using Negometrix.Services.DTOs;
using Negometrix.Services.Exceptions;
using Negometrix.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negometrix.Tests.FormServices_Should
{
    [TestClass]
   public  class GetUserForms_Should : BaseTest
    {
        [TestMethod]
        public async Task GetUserForms_WithCorrectData()
        {
            var options = Utility.GetOptions(nameof(GetUserForms_WithCorrectData));

            using (var contextConfig = new NegometrixContext(options))
            {


                var user = new User
                {
                    UserName = "testUser",
                    Id = 1,
                };

                contextConfig.Users.Add(user);

                await contextConfig.SaveChangesAsync();
                var questionType = new QuestionType
                {
                    QuestionTypeId = 1,
                    QuestionTypeName = "text"
                };

                var question = new Question()
                {
                    QuestionId = 1,
                    QuestionType = questionType
                };

                var listQ = new List<Question>();
                listQ.Add(question);

                var form = new Form
                {
                    UserId =1,
                    FormId = Guid.NewGuid(),
                    FormDescription = null,
                    IsDeleted = false,
                    Questions = listQ
                };
                var form2 = new Form
                {
                    UserId = 1,
                    FormId = Guid.NewGuid(),
                    FormDescription = null,
                    IsDeleted = false,
                    Questions = listQ
                };
                var form3 = new Form
                {
                    UserId = 2,
                    FormId = Guid.NewGuid(),
                    FormDescription = null,
                    IsDeleted = false,
                    Questions = listQ
                };
                await contextConfig.AddAsync(form);
                await contextConfig.AddAsync(form2);
                await contextConfig.AddAsync(form3);

                await contextConfig.SaveChangesAsync();

                using (var assertContext = new NegometrixContext(options))
                {
                    var expectedQuestion = new Question()
                    {
                        QuestionId = 1,
                        QuestionType = questionType
                    };

                    var myProfile = new DTOAutoMapper();
                    var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
                    var mapper = new Mapper(configuration);
                    var expected = mapper.Map<ICollection<FormDTO>>(contextConfig.Forms.ToList());

                    var sut = new FormService(mapper, contextConfig, dateTimeProvider);
                    var actual = await sut.GetUserForms(1);

                    Assert.AreEqual(expected.Count-1, actual.Count);
                    Assert.AreEqual(expected.First().UserId, actual.First().UserId);
                    Assert.AreEqual(expected.Skip(1).First().UserId,
                        actual.Last().UserId);
                }

            }
        }

        [TestMethod]
        public async Task GetUserForm_WithWrongId()
        {
            var options = Utility.GetOptions(nameof(GetUserForm_WithWrongId));

            using (var contextConfig = new NegometrixContext(options))
            {


                var user = new User
                {
                    UserName = "testUser",
                    Id = 1,
                };

                contextConfig.Users.Add(user);

                await contextConfig.SaveChangesAsync();
                var questionType = new QuestionType
                {
                    QuestionTypeId = 1,
                    QuestionTypeName = "text"
                };

                var question = new Question()
                {
                    QuestionId = 1,
                    QuestionType = questionType
                };

                var listQ = new List<Question>();
                listQ.Add(question);

                var form = new Form
                {
                    FormId = Guid.NewGuid(),
                    FormDescription = null,
                    IsDeleted = false,
                    Questions = listQ
                };
                await contextConfig.AddAsync(form);
                await contextConfig.SaveChangesAsync();



                using (var assertContext = new NegometrixContext(options))
                {

                    var myProfile = new DTOAutoMapper();
                    var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
                    var mapper = new Mapper(configuration);

                    var sut = new FormService(mapper, contextConfig, dateTimeProvider);
                    var actual = await sut.GetUserForms(-1);

                    Assert.AreEqual(0, actual.Count);

                }

            }
        }
    }
}
