﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Negometrix.Data;
using Negometrix.Data.Entities;
using Negometrix.Services.Contracts;
using Negometrix.Services.DTOs;
using Negometrix.Services.Exceptions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Negometrix.Services.Services
{
    public class AnswerFormService : IAnswerFormService
    {
        private readonly IMapper mapper;
        private readonly NegometrixContext dbcontext;
        private readonly IDateTimeProvider dateTimeProvider;

        public AnswerFormService(IMapper mapper,
                           NegometrixContext dbcontext,
                           IDateTimeProvider dateTimeProvider)
        {
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            this.dbcontext = dbcontext ?? throw new ArgumentNullException(nameof(dbcontext));
            this.dateTimeProvider = dateTimeProvider ?? throw new ArgumentNullException(nameof(dateTimeProvider));
        }

        public async Task<AnswerFormDTO> CreateAnswerForm(AnswerFormDTO answerFormDTO)
        {
            await ValidateFormExist(answerFormDTO.FormId);
            var answerForm = mapper.Map<AnswerForm>(answerFormDTO);
            answerForm.CreatedOn = dateTimeProvider.GetDateTime();

            for (int i = 0; i < answerForm.Answers.Count; i++)
            {
                var notMappedQuestion = answerForm.Answers.ToList()[i].Question.QuestionId;
                var mappedQuestion = await dbcontext.Questions.FirstOrDefaultAsync(a => a.QuestionId == notMappedQuestion);

                answerForm.Answers.ToList()[i].Question = mappedQuestion;
            }

            await dbcontext.AnsweredForms.AddAsync(answerForm);

            try
            {
                await dbcontext.SaveChangesAsync();

                var answerFormDTOMapped = mapper.Map<AnswerFormDTO>(answerForm);
                return answerFormDTOMapped;
            }
            catch (ValidationException e)
            {
                throw new InvalidParameterException(e.Message);
            }
        }

        private async Task ValidateFormExist(Guid formId)
        {
            var formExist = await dbcontext.Forms.AnyAsync(form => form.FormId == formId);

            if (!formExist)
            {
                throw new ResourceNotExistException($"There is no form with id: {formId}");
            }
        }
        
        private async Task ValidateFormNotDeleted(Guid formId)
        {
            var formExist = await dbcontext.Forms.AnyAsync(form => form.IsDeleted == false && form.FormId == formId );

            if (!formExist)
            {
                throw new ResourceNotExistException($"The form with id: {formId} is deleted!");
            }
        }

        public async Task<ICollection<AnswerFormDTO>> GetAnswerForms(Guid formId)
        {
            await ValidateFormExist(formId);
            await ValidateFormNotDeleted(formId);

            var answerFormsEntity = await dbcontext.AnsweredForms.Include(af => af.Answers)
                                                                 .ThenInclude(a => a.Question)
                                                                 .ThenInclude(q => q.QuestionType)
                                                                 .Where(answer => answer.FormId == formId).ToListAsync();

            var answerFormsDTO = mapper.Map<ICollection<AnswerFormDTO>>(answerFormsEntity);

            return answerFormsDTO;
        }

        public async Task<AnswerFormDTO> GetAnswerForm(Guid answerFormId)
        {
            var answerFormsEntity = await dbcontext.AnsweredForms.Include(af => af.Answers)
                                                                 .ThenInclude(a => a.Question)
                                                                 .ThenInclude(q => q.QuestionType)
                                                                 .FirstOrDefaultAsync(answerForm => answerForm.AnswerFormId == answerFormId);
            var answerFormsDTO = mapper.Map<AnswerFormDTO>(answerFormsEntity);

            return answerFormsDTO;
        }
    }
}
