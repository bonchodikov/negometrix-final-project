﻿using Microsoft.AspNetCore.StaticFiles;

namespace Negometrix.Services.Services.Blob_Service.Extensions
{
    public static class FileExtensions
    {
        private static readonly FileExtensionContentTypeProvider Provider = new FileExtensionContentTypeProvider();

        public static string GetContentType(this string fileName)
        {
            if (!Provider.TryGetContentType(fileName, out var contentType))
            {
                contentType = "application/octec-stream";
            }
            return contentType;
        }
    }
}
