﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Negometrix.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Negometrix.Data.ConfigurationModels
{
    public class FormConfig : IEntityTypeConfiguration<Form>
    {
        public void Configure(EntityTypeBuilder<Form> builder)
        {
            builder.HasKey(p => p.FormId);

            builder.HasOne(p => p.User).WithMany(p => p.Forms).OnDelete(DeleteBehavior.NoAction);

            //builder.Property(p => p.VersionId);

            builder.Property(p => p.FormName).
                                   IsRequired().
                                   HasMaxLength(150);

            builder.Property(p => p.FormDescription);

            builder.Property(p => p.CreatedOn)
                                        .IsRequired();

            builder.HasMany(p=>p.AnsweredForms);

            builder.HasMany(p=>p.Questions);

            builder.Property(p => p.IsDeleted);

            builder.Property(p => p.DeletedOn);

            builder.Property(p => p.ModifiedOn);
        }
    }
}
