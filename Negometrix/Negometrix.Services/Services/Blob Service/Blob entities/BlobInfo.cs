﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Negometrix.Services.Services.Blob_Service.Blob_entities
{
    public class BlobInfo
    {
        public BlobInfo(Stream content, string contentType)
        {
            this.Content = content;
            this.ContentType = contentType;
        }

        public Stream Content { get; set; }
        public string ContentType { get; set; }
    }
}
