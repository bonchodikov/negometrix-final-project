﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Negometrix.Data.Entities.Contracts
{
    public interface IEntities
    {
        DateTime CreatedOn { get; set; }
        DateTime? ModifiedOn { get; set; }
        bool IsDeleted { get; set; }
    }
}
