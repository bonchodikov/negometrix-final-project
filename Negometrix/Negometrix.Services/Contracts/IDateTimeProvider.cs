﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Negometrix.Services.Contracts
{
    public interface IDateTimeProvider
    {
        DateTime GetDateTime();
    }
}
