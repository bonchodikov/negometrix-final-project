﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Negometrix.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Negometrix.Data.ConfigurationModels
{
    public class QuestionTypeConfig : IEntityTypeConfiguration<QuestionType>
    {
        public void Configure(EntityTypeBuilder<QuestionType> builder)
        {
            builder.HasKey(p => p.QuestionTypeId);

            builder.Property(p => p.QuestionTypeName).
                                        HasMaxLength(20);
        }
    }
}
