﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Negometrix.Services.DTOs
{
    public class QuestionDTO
    {
        public int QuestionId { get; set; }
        public Guid FormId { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public bool isRequired { get; set; }
        public QuestionTypeDTO QuestionType { get; set; }
        public bool IsMultiple { get; set; }
        public bool IsLong { get; set; }
        public int MaxSize { get; set; }
        public int NumberOfFiles { get; set; }
        public string[] Options { get; set; }
    }
}
