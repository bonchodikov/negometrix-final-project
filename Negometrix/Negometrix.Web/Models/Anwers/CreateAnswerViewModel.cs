﻿using Microsoft.AspNetCore.Http;
using Negometrix.Services.DTOs;
using Negometrix.Web.Models.Questions;
using Negometrix.Web.Models.Questions.QuestionTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Negometrix.Web.Models.Anwers
{
    public class CreateAnswerViewModel
    {
        public CreateAnswerViewModel()
        {
            OptionsResult = new List<string>();
            FileLinksResult = new List<string>();
        }
        public ICollection<IFormFile> Files{ get; set; }
        public string TextResult { get; set; }
        public List<string> OptionsResult { get; set; }
        public List<string> FileLinksResult { get; set; }
        public int AnswerId { get; set; }
        public Guid AnswerFormId { get; set; }
        public CreateQuestionViewModel Question { get; set; }
    }
}
