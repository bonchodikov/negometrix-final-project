using AutoMapper;
using Azure.Storage.Blobs;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Negometrix.Data;
using Negometrix.Data.Entities;
using Negometrix.Infrastructure.Middlewares;
using Negometrix.Services;
using Negometrix.Services.Contracts;
using Negometrix.Services.DTOMappers;
using Negometrix.Services.Services;
using Negometrix.Services.Services.Blob_Service;
using Negometrix.Services.Services.Blob_Service.Contracts;
using Negometrix.Web.Validators;
using NToastNotify;
using System;

namespace Negometrix.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<NegometrixContext>(options =>
            options.UseSqlServer(
                 Configuration.GetConnectionString("DefaultConnection")));


            // Add and Configuring the Sessions
            services.AddSession(options =>
            {
                options.Cookie.HttpOnly = true; // XSS Security [no JS]

                // Session Timeout 
                // For Testing => Set a short Timeout
                options.IdleTimeout = new TimeSpan(0, 0, 0, 10); // Days, Hours, Minutes, Seconds
            });

            services.AddDefaultIdentity<User>(options =>
            {
                options.SignIn.RequireConfirmedEmail = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false; options.Password.RequireDigit = true;
                options.Password.RequiredUniqueChars = 0;
                options.Password.RequiredLength = 6;
            }).AddRoles<Role>().AddEntityFrameworkStores<NegometrixContext>();

            services.AddControllersWithViews()
                .AddFluentValidation(cfg=>cfg
                .RegisterValidatorsFromAssemblyContaining<CreateQuestionViewModelValidator>());

            services.AddRazorPages();

            services.AddMvc().AddNToastNotifyToastr(new ToastrOptions()
            {
                ProgressBar = false,
                PositionClass = ToastPositions.TopFullWidth
            });

            services.AddAutoMapper(typeof(Startup), typeof(DTOAutoMapper));

            services.AddSingleton(x => new BlobServiceClient(Configuration.GetValue<string>("AzureBlobStorageConnectionString")));
            services.AddSingleton<IBlobService, BlobService>();
           
            services.AddScoped<IDateTimeProvider, DateTimeProvider>();
            services.AddScoped<IFormService, FormService>();
            services.AddScoped<IAnswerFormService, AnswerFormService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days.You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseNToastNotify();

            app.UseMiddleware<ModelSeederMiddleware>();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "areas",
                    pattern: "{area:exists}/{controller=Home}/{action=Index}/{id?}");

                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");

                endpoints.MapRazorPages();
            });
        }
    }
}
