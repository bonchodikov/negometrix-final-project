﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Negometrix.Web.Models
{
    public class FormViewModel
    {
        public Guid FormId { get; set; }
        public string FormName { get; set; }
        public string FormDescription { get; set; }
        public int UserId { get; set; }

        public DateTime? ModifiedOn { get; set; }
    }
}
