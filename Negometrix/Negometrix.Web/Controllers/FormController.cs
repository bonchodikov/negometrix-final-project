﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Negometrix.Services.Contracts;
using Negometrix.Services.DTOs;
using Negometrix.Web.Models;
using NToastNotify;
using System.Linq;
using Negometrix.Web.Models.Questions;
using System.Threading.Tasks;
using System.Threading;

namespace Negometrix.Web.Controllers
{
    [Authorize(Roles = "Member,Admin")]
    public class FormController : Controller
    {
        private readonly IMapper mapper;
        private readonly IFormService formService;
        private readonly IDateTimeProvider dateTimeProvider;
        private readonly IToastNotification toastNotification;

        public FormController(IMapper mapper,
                              IFormService formService,
                              IDateTimeProvider dateTimeProvider,
                              IToastNotification toastNotification)
        {
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            this.dateTimeProvider = dateTimeProvider ?? throw new ArgumentNullException(nameof(dateTimeProvider));
            this.formService = formService ?? throw new ArgumentNullException(nameof(formService));
            this.toastNotification = toastNotification ?? throw new ArgumentNullException(nameof(toastNotification));
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var userId = Convert.ToInt32(this.User.FindFirstValue(ClaimTypes.NameIdentifier));
            var formDTOs = await formService.GetUserForms(userId);

            var formViewModels = mapper.Map<ICollection<ListFormViewModel>>(formDTOs);

            return View("ListAllForms", formViewModels);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new CreateFormViewModel();
            return View("CreateForm", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("FormName,FormDescription,Questions")] CreateFormViewModel form)
        {
            if (ModelState.IsValid)
            {
                var formDTO = mapper.Map<FormDTO>(form);
                var userId = Convert.ToInt32(this.User.FindFirstValue(ClaimTypes.NameIdentifier));
                formDTO.UserId = userId;
                var currentFormDTO = await formService.CreateForm(formDTO);

                var formModel = mapper.Map<FormViewModel>(currentFormDTO);
                return RedirectToAction("Details", "Form", formModel);
            }
            return View("CreateForm", form);
        }

        //[Route("Share")]
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Share(string[] data, Guid formId)
        //{
        //    var messageSend = await formService.SendMail(data, formId);
        //    return RedirectToAction("Index", "AnswerForm", new { id = formId });
        //}

        [HttpGet]
        public IActionResult Details(FormViewModel form)
        {
            var userId = Convert.ToInt32(this.User.FindFirstValue(ClaimTypes.NameIdentifier));
            form.UserId = userId;

            this.toastNotification.AddSuccessToastMessage("The form is created successfuly!");
            return View("FormDetails", form);
        }

        [HttpGet]
        public async Task<IActionResult> DetailsList(Guid id)
        {
                
                var formDTO =await formService.GetForm(id);
                var formModel = mapper.Map<FormViewModel>(formDTO);

                //var userId = Convert.ToInt32(this.User.FindFirstValue(ClaimTypes.NameIdentifier));
                //formModel.UserId = userId;

                return View("FormDetails", formModel);
        }


        [HttpPost] 
        [ValidateAntiForgeryToken]
        public IActionResult AddQuestion([Bind("Questions")] CreateFormViewModel form)
        {
            form.Questions.Add(new CreateQuestionViewModel());
            ModelState.Clear();
            return PartialView("CreateQuestionViewModels", form);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult RemoveQuestion([Bind("Questions")] CreateFormViewModel form)
        {
            var removeElement = form.Questions.FirstOrDefault(p => p.IsDeleted.Equals(true));
            form.Questions.Remove(removeElement);

            ModelState.Clear();
            return PartialView("CreateQuestionViewModels", form);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult RemoveQuestionOption([Bind("Questions")] CreateFormViewModel form)
        {
            var removeElement = form.Questions.FirstOrDefault(p => p.ChoseOptionToRemove != null);
            var index = removeElement.ChoseOptionToRemove.Split('.').Select(int.Parse).ToArray();
            var removedOpt = form.Questions[index[0]].Options[index[1]];
            form.Questions[index[0]].Options.Remove(removedOpt);

            ModelState.Clear();
            return PartialView("CreateQuestionViewModels", form);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(Guid id)
        {
            var formToEdit = await formService.GetForm(id);
            return View("CreateForm", mapper.Map<CreateFormViewModel>(formToEdit));
        }
    }
}