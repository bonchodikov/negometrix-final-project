﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Negometrix.Web.Models
{
    public class ListFormViewModel
    {
        public Guid FormId { get; set; }
        public string FormName { get; set; }
        public string FormDescription { get; set; }
        public int QuestionsCount { get; set; }

        public DateTime CreatedOn { get; set; }
        public DateTime? DeletedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }

    }
}
