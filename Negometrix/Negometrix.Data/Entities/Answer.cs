﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Negometrix.Data.Entities
{
    public class Answer
    {
        public string TextResult { get; set; }
        public string[] OptionsResult { get; set; }
        public string[] FileLinksResult { get; set; }
        public int AnswerId { get; set; }
        public Guid AnswerFormId { get; set; }

        public Question Question { get; set; }
        public int QuestionId { get; set; }
    }
}
