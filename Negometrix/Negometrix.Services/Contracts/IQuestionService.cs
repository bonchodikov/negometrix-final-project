﻿using Negometrix.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Negometrix.Services.Contracts
{
    public interface IQuestionService
    {
        QuestionDTO GetQuestion(int formId, int versionId, int questionId);
        QuestionDTO ListQuestions(int formId, int versionId);
        QuestionDTO AddQuestion(QuestionDTO questionDTO);
        ICollection<QuestionTypeDTO> GetQuestionTypes();
    }
}
