﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Negometrix.Web.Models.AnswerForms
{
    public class ListAnswerFormViewModel
    {
        public Guid FormId { get; set; }
        public Guid AnswerFormId { get; set; }
        public string AnswerFormName { get; set; }
        public string AnswerFormDescription { get; set; }
        public int QuestionsCount { get; set; }

        public DateTime CreatedOn { get; set; }
    }
}
