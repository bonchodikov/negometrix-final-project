﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Negometrix.Data.Entities
{
    public class QuestionType
    {
        public QuestionType()
        {
            Questions = new List<Question>();            
        }

        public int QuestionTypeId { get; set; }
        public string QuestionTypeName { get; set; }
        public ICollection<Question> Questions { get; set; }


    }
}
