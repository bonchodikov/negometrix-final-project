﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Negometrix.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IdentityRole = Microsoft.AspNetCore.Identity.IdentityRole;

namespace Negometrix.Infrastructure.Middlewares
{

    public class ModelSeederMiddleware
    {
        private readonly RequestDelegate _next;

        public ModelSeederMiddleware(RequestDelegate next)
        {
            this._next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext, RoleManager<Role> roleManager, UserManager<User> userManager)
        {
            if (!await roleManager.RoleExistsAsync("Admin"))
            {
                var role = new Role();
                role.Name = "Admin";
                //TO DO: INSPECT ID FIRST CREATED USER
                await roleManager.CreateAsync(role);
            }
            else if (!await roleManager.RoleExistsAsync("Member"))
            {
                var role = new Role();
                role.Name = "Member";

                await roleManager.CreateAsync(role);
            }
            if (!userManager.Users.Any(user => user.UserName == "admin"))
            {
                var user = new User { UserName = "admin" };
                IdentityResult adminCreationResult = await userManager.CreateAsync(user: user, password: "admin1");

                if (adminCreationResult.Succeeded)
                {
                    await userManager.AddToRoleAsync(user, "Admin");
                }
            }
            await _next(httpContext);
        }
    }
}
