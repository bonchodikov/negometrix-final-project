﻿using AutoMapper;
using Negometrix.Data.Entities;
using Negometrix.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Negometrix.Services.DTOMappers
{
    public class DTOAutoMapper : Profile
    {
        public DTOAutoMapper() 
        {
            CreateMap<Form, FormDTO>().ReverseMap();

            CreateMap<Answer, AnswerDTO>().ReverseMap();

            CreateMap<FormDTO, AnswerFormDTO>().ReverseMap();

            CreateMap<AnswerForm, AnswerFormDTO>().ReverseMap();

            CreateMap<Question, QuestionDTO>().ReverseMap();

            CreateMap<QuestionType, QuestionTypeDTO>().ReverseMap();

            CreateMap<AnswerDTO, QuestionDTO>().ReverseMap().ForMember(dest=>dest.Question, opt=>opt.MapFrom(src=>src));

            CreateMap<User, UserDTO>().ReverseMap();

            CreateMap<FormDTO, AnswerFormDTO>()
                .ForMember(dest=>dest.AnswerFormName, opt=>opt.MapFrom(src=>src.FormName))
                .ForMember(dest=>dest.AnswerFormDescription,opt=>opt.MapFrom(src=>src.FormDescription))
                .ForMember(dest => dest.Answers, opt => opt.MapFrom(src => src.Questions)).ReverseMap();

            CreateMap<AnswerDTO, AnswerFormDTO>().ReverseMap();

            //CreateMap<FormDTO, AnsweredFormDTO>().ForMember(dest => dest.Answers, opt => opt.MapFrom(src => src.Questions)).ReverseMap();
        }
    }
}
