﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Negometrix.Data;
using Negometrix.Data.Entities;
using Negometrix.Services.DTOMappers;
using Negometrix.Services.DTOs;
using Negometrix.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negometrix.Tests.FormServices_Should
{
    [TestClass]
    public class GetForm_Should : BaseTest
    {
        [TestMethod]
        public async Task GetCorrectForm_WithCorrectData()
        {
            var options = Utility.GetOptions(nameof(GetCorrectForm_WithCorrectData));

            using (var contextConfig = new NegometrixContext(options))
            {


                var user = new User
                {
                    UserName = "testUser",
                    Id = 1,
                };

                contextConfig.Users.Add(user);

                await contextConfig.SaveChangesAsync();
                var questionType = new QuestionType
                {
                    QuestionTypeId = 1,
                    QuestionTypeName = "text"
                };

                var question = new Question()
                {
                    QuestionId = 1,
                    QuestionType = questionType
                };

                var listQ = new List<Question>();
                listQ.Add(question);

                var form = new Form
                {
                    FormId = Guid.NewGuid(),
                    FormDescription = null,
                    IsDeleted = false,
                    Questions = listQ
                };
                await contextConfig.AddAsync(form);
                await contextConfig.SaveChangesAsync();

                using (var assertContext = new NegometrixContext(options))
                {
                    var expectedQuestion = new Question()
                    {
                        QuestionId = 1,
                        QuestionType = questionType
                    };

                    var myProfile = new DTOAutoMapper();
                    var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
                    var mapper = new Mapper(configuration);
                    var expected = mapper.Map<FormDTO>(form);

                    var sut = new FormService(mapper, contextConfig, dateTimeProvider);
                    var actual = await sut.GetForm(form.FormId);

                    Assert.AreEqual(expected.FormId, actual.FormId);
                    Assert.AreEqual(expected.Questions.Count, actual.Questions.Count);
                    Assert.AreEqual(expected.Questions.FirstOrDefault().QuestionType.QuestionTypeName,
                        actual.Questions.FirstOrDefault().QuestionType.QuestionTypeName);
                }

            }
        }
        [TestMethod]
        public async Task GetForm_WithWrongId()
        {
            var options = Utility.GetOptions(nameof(GetForm_WithWrongId));

            using (var contextConfig = new NegometrixContext(options))
            {


                var user = new User
                {
                    UserName = "testUser",
                    Id = 1,
                };

                contextConfig.Users.Add(user);

                await contextConfig.SaveChangesAsync();
                var questionType = new QuestionType
                {
                    QuestionTypeId = 1,
                    QuestionTypeName = "text"
                };

                var question = new Question()
                {
                    QuestionId = 1,
                    QuestionType = questionType
                };

                var listQ = new List<Question>();
                listQ.Add(question);

                var form = new Form
                {
                    FormId = Guid.NewGuid(),
                    FormDescription = null,
                    IsDeleted = false,
                    Questions = listQ
                };
                await contextConfig.AddAsync(form);
                await contextConfig.SaveChangesAsync();



                using (var assertContext = new NegometrixContext(options))
                {

                    var myProfile = new DTOAutoMapper();
                    var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
                    var mapper = new Mapper(configuration);

                    var sut = new FormService(mapper, contextConfig, dateTimeProvider);
                    var actual = await sut.GetForm(Guid.NewGuid());

                    Assert.AreEqual(null, actual);

                }

            }
        }
    }
}
