﻿using Microsoft.AspNetCore.Builder;
using Negometrix.Infrastructure.Middlewares;
using System;

namespace Negometrix.Infrastructure
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder AddDefaultAdminAccount(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ModelSeederMiddleware>();
        }
    }
}
