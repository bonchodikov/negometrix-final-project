﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Negometrix.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Negometrix.Data.ConfigurationModels
{
    public class QuestionConfig : IEntityTypeConfiguration<Question>
    {
        public void Configure(EntityTypeBuilder<Question> builder)
        {
            builder.HasKey(p => p.QuestionId);

            builder.HasOne(p => p.QuestionType);
      
         
            builder.Property(p => p.CreatedOn).
                                    IsRequired();

            builder.Property(p => p.IsRequired).
                                    IsRequired();

            builder.Property(p => p.ModifiedOn);

            builder.Property(p => p.DeletedOn);

            builder.Property(p => p.IsDeleted).
                                    IsRequired();

        }
    }
}
