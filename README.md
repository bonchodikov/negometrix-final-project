<img align="center"  src="/Images/company-logo-documentation.png" width="200" height="200">

## **Project Description**<br />
Welcome to <b>Negometrix's final project</b>, a web application for managing different types of questionnaires!<br />

The mission of our project is to allow users to share custom forms to everyone with an additional options for viewing those answers and modifying the questionnaires<br/>
Our project's description is given by the Negometrix and our goal is to satisfy the following things: 
<br />
- [x] Areas:
  - public part (accessible without authentication)
  - private part (available for registered users)

## **Preliminary Information**
This project was assigned to <b>team #20 </b> during Alpha .NET Jan'20 Module IV at **Telerik Academy**.<br>

|<img src="/Images/telerik-logo.png" width="200">|<div align="center"> <img src="/Images/education.png" width="200"></div>|<img src="/Images/trello-logo.png" width="200">|
|:------------:|:------------:|:------------:|
|[**Telerik Academy**](https://www.telerikacademy.com/)|[**Ivaylo Gramatikov**](https://gitlab.com/xzikoto)<br> [**Boncho Dikov**](https://gitlab.com/bonchodikov)|[**"Negometrix" Team Project**](https://trello.com/b/KlwJpcsE/final-project-negometrix)



