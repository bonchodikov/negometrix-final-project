﻿using Negometrix.Web.Models.Questions.QuestionTypes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Negometrix.Web.Models.Questions
{
    public class CreateQuestionViewModel
    {
        public CreateQuestionViewModel()
        {
            QuestionType = new QuestionTypeViewModel();
            QuestionType.QuestionTypeName = "none";
            Options = new List<string>();
        }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        [MaxLength(50)]
        public string Description { get; set; }

        public Guid FormId { get; set; }
        public int QuestionId { get; set; }
        public bool IsRequired { get; set; }
        public bool IsLong { get; set; }
        public bool IsMultiple { get; set; }
        public int MaxSize { get; set; }
        public int NumberOfFiles { get; set; }
        public QuestionTypeViewModel QuestionType { get; set; }
        public List<string> Options { get; set; }
        public bool IsDeleted { get; set; }
        public string ChoseOptionToRemove { get; set; }
    }
}
