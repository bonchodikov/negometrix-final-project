﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Negometrix.Services.Contracts;
using Negometrix.Services.DTOs;
using Negometrix.Services.Exceptions;
using Negometrix.Services.Services.Blob_Service.Blob_entities;
using Negometrix.Services.Services.Blob_Service.Contracts;
using Negometrix.Web.Models.AnswerForms;
using NToastNotify;

namespace Negometrix.Web.Controllers
{
    [Route("AnswerForm")]
    public class AnswerFormController : Controller
    {
        private readonly IMapper mapper;
        private readonly IFormService formService;
        private readonly IAnswerFormService answerFormService;
        private readonly IBlobService blobService;
        private readonly IDateTimeProvider dateTimeProvider;
        private readonly IToastNotification toastNotification;

        public AnswerFormController(IMapper mapper,
                                    IFormService formService,
                                    IAnswerFormService answerFormService,
                                    IBlobService blobService,
                                    IDateTimeProvider dateTimeProvider,
                                    IToastNotification toastNotification)
        {
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            this.formService = formService ?? throw new ArgumentNullException(nameof(formService));
            this.answerFormService = answerFormService ?? throw new ArgumentNullException(nameof(answerFormService));
            this.blobService = blobService ?? throw new ArgumentNullException(nameof(blobService));
            this.dateTimeProvider = dateTimeProvider ?? throw new ArgumentNullException(nameof(dateTimeProvider));
            this.toastNotification = toastNotification ?? throw new ArgumentNullException(nameof(toastNotification));
        }

        [HttpGet("Index/{id}")]
        public async Task<IActionResult> Index(Guid id)
        {
            var formDTO = await formService.GetForm(id);

            var answerFormDTO = mapper.Map<AnswerFormDTO>(formDTO);
            var result = mapper.Map<CreateAnswerFormViewModel>(answerFormDTO);

            return View("CreateAnswerForm", result);
        }
        [Route("Share")]
        [HttpPost]
        public async Task<IActionResult> Share(string[] data, Guid formId)
        {
            var messageSend = await formService.SendMail(data, formId);
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> CreateAnswerForm(CreateAnswerFormViewModel model)
        {
            if (ModelState.IsValid)
            {
                var filesExist = model.Answers.Any(answer => answer.Files != null);

                if (filesExist)
                {
                    foreach (var item in model.Answers)
                    {
                        if (item.Files != null)
                        {
                            foreach (var blob in item.Files)
                            {
                                await blobService.UploadAssetAsync(blob);
                                string URL = await blobService.GetBlobAsync(blob.FileName);
                                item.FileLinksResult.Add(URL);
                            }
                        }

                        item.Files = null;
                    }

                }
                try
                {
                    var answerFormDTO = await answerFormService.CreateAnswerForm(mapper.Map<AnswerFormDTO>(model));
                    var answerFormModel = mapper.Map<AnswerFormViewModel>(answerFormDTO);

                    return View("AnswerFormCreated");
                }
                catch (InvalidParameterException e)
                {
                    throw new InvalidParameterException(e.Message);
                }
            }

            return View("CreateAnswerForm", model);
        }

        
        [HttpGet]
        [Route("Answers/{id}")]
        public async Task<IActionResult> Answers(Guid id)
        {
            var answerFormsDTO = await answerFormService.GetAnswerForms(id);
            var answerFormsViewModels = mapper.Map<ICollection<ListAnswerFormViewModel>>(answerFormsDTO);

            return View("ListAllAnswerForms", answerFormsViewModels);
        }

        [HttpGet]
        [Route("PreviewAnswer/{AnswerFormId}")]
        public async Task<IActionResult> PreviewAnswer(Guid AnswerFormId)
        {
            var answerFormsDTO = await answerFormService.GetAnswerForm(AnswerFormId);
            var answerFormsViewModel = mapper.Map<CreateAnswerFormViewModel>(answerFormsDTO);

            return View("PreviewAnsweredForm", answerFormsViewModel);
        }

    }
}