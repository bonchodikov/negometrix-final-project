﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Negometrix.Services.DTOs
{
    public class UserDTO
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public ICollection<FormDTO> Forms { get; set; }
        public ICollection<AnswerFormDTO> AnsweredForms { get; set; }
    }
}
