﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Negometrix.Data.Entities.Contracts.QuestionContracts
{
    public interface IQuestion : IEntities
    {
        int QuestionId { get; set; }
        int QuestionTypeId { get; set; }
        QuestionType QuestionType { get; set; }

    }
}
