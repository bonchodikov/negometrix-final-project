﻿using Microsoft.EntityFrameworkCore;
using Negometrix.Data.Entities;
using System.Linq;

namespace Negometrix.Data.Seeder
{
    public static class ModelBuilderExtension
    {
        public static void RelationshipSetter(this ModelBuilder model)
        {
            foreach (var relationship in model.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
        }

        public static void Seeder(this ModelBuilder builder)
        {
            builder.Entity<QuestionType>().HasData(

                new QuestionType
                {
                    QuestionTypeId=1,
                    QuestionTypeName = "Single"
                },

                new QuestionType
                {
                    QuestionTypeId = 2,
                    QuestionTypeName = "Multiple "
                },

                new QuestionType
                {
                    QuestionTypeId = 3,
                    QuestionTypeName = "Text"
                },

                new QuestionType
                {
                    QuestionTypeId = 4,
                    QuestionTypeName = "Document"
                }
            );
        }
    }
}
