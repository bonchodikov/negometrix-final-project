﻿using Microsoft.AspNetCore.Http;
using Negometrix.Services.Services.Blob_Service.Blob_entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Negometrix.Services.Services.Blob_Service.Contracts
{
    public interface IBlobService
    {
        public Task<string> GetBlobAsync(string name);

        public Task<IEnumerable<string>> ListBlobsAsync();

        public Task UploadFileBlobAsync(string filePath, string fileName);

        public Task UploadContentBlobAsync(string content, string fileName);

        public Task DeleteBlobAsync(string blobName);

        public Task UploadAssetAsync(IFormFile asset);
    }
}
