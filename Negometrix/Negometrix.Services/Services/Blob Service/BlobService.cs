﻿using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Negometrix.Services.Services.Blob_Service.Blob_entities;
using Negometrix.Services.Services.Blob_Service.Contracts;
using Negometrix.Services.Services.Blob_Service.Extensions;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using BlobInfo = Negometrix.Services.Services.Blob_Service.Blob_entities.BlobInfo;

namespace Negometrix.Services.Services.Blob_Service
{
    public class BlobService : IBlobService
    {
        private readonly BlobServiceClient _blobServiceClient;
        private readonly IConfiguration _configuration;
        public BlobService(BlobServiceClient blobServiceClient, IConfiguration config)
        {
            _configuration = config;
            _blobServiceClient = blobServiceClient;
        }

        public async Task<string> GetBlobAsync(string name)
        {
            var containerClient = _blobServiceClient.GetBlobContainerClient("assets");
            var blobClient = containerClient.GetBlobClient(name);
            var blobDownloadInfo = await blobClient.DownloadAsync();

            return blobClient.Uri.AbsoluteUri;
        }

        public async Task UploadContentBlobAsync(string content, string fileName)
        {
            var containerClient = _blobServiceClient.GetBlobContainerClient("assets");
            var blobClient = containerClient.GetBlobClient(fileName);

            var bytes = Encoding.UTF8.GetBytes(content);
            await using var memoryStream = new MemoryStream(bytes);

            await blobClient.UploadAsync(memoryStream, new BlobHttpHeaders { ContentType = fileName.GetContentType() });
        }

        public async Task UploadAssetAsync(IFormFile asset)
        {

            var container = _blobServiceClient.GetBlobContainerClient("assets");
            var client = container.GetBlobClient(asset.FileName);

            var blob = container.GetBlobClient(asset.FileName);
            await blob.UploadAsync(asset.OpenReadStream());
        }

        public async Task UploadFileBlobAsync(string filePath, string fileName)
        {
            var containerClient = _blobServiceClient.GetBlobContainerClient("assets");
            var blobClient = containerClient.GetBlobClient(fileName);

            await blobClient.UploadAsync(filePath, new BlobHttpHeaders { ContentType = filePath.GetContentType() });
        }

        public async Task<IEnumerable<string>> ListBlobsAsync()
        {
            var containerClient = _blobServiceClient.GetBlobContainerClient("assets");
            var items = new List<string>();

            await foreach (var blobItem in containerClient.GetBlobsAsync())
            {
                items.Add(blobItem.Name);
            }

            return items;
        }




        public async Task DeleteBlobAsync(string blobName)
        {
            var containerClient = _blobServiceClient.GetBlobContainerClient("assets");
            var blobClient = containerClient.GetBlobClient(blobName);

            await blobClient.DeleteIfExistsAsync();
        }
    }
}
