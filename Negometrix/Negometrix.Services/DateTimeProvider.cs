﻿using Negometrix.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Negometrix.Services
{
    public class DateTimeProvider : IDateTimeProvider
    {
        public DateTime GetDateTime() => DateTime.UtcNow;
    }
}
