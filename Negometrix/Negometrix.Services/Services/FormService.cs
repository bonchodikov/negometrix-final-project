﻿using AutoMapper;
using Negometrix.Data;
using Negometrix.Data.Entities;
using Negometrix.Services.Contracts;
using Negometrix.Services.DTOs;
using Negometrix.Services.Exceptions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using MailKit.Net.Smtp;
using MimeKit;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Negometrix.Services.Services
{
    public class FormService : IFormService
    {
        private readonly IMapper mapper;
        private readonly NegometrixContext dbcontext;
        private readonly IDateTimeProvider dateTimeProvider;

        public FormService(IMapper mapper,
                           NegometrixContext dbcontext,
                           IDateTimeProvider dateTimeProvider)
        {
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            this.dbcontext = dbcontext ?? throw new ArgumentNullException(nameof(dbcontext));
            this.dateTimeProvider = dateTimeProvider ?? throw new ArgumentNullException(nameof(dateTimeProvider));
        }

        public async Task<FormDTO> GetForm(Guid id) 
        {
            var form = await dbcontext.Forms.Include(form => form.Questions)
                                      .ThenInclude(question => question.QuestionType)
                                      .FirstOrDefaultAsync(form => form.FormId == id && form.IsDeleted == false);

            var formDTO = mapper.Map<FormDTO>(form);

            return formDTO;
        }

        public FormDTO GetFormNoAsync(Guid id) 
        {
            var form = dbcontext.Forms.Include(form => form.Questions)
                                      .ThenInclude(question => question.QuestionType)
                                      .FirstOrDefaultAsync(form => form.FormId == id && form.IsDeleted == false);

            var formDTO = mapper.Map<FormDTO>(form);

            return formDTO;
        }

        public async Task<FormDTO>  CreateForm(FormDTO formDTO) 
        {
           await ValidateUserExist(formDTO.UserId);

            var form = mapper.Map<Form>(formDTO);
            form.CreatedOn = dateTimeProvider.GetDateTime();

            await dbcontext.Forms.AddAsync(form);

            try
            {
                await dbcontext.SaveChangesAsync();

                var formDTOMapped = mapper.Map<FormDTO>(form);
                return formDTOMapped;
            }
            catch (ValidationException e)
            {
                throw new InvalidParameterException(e.Message);
            }
        }

        private async Task ValidateUserExist(int userId)
        {
            var userExist = await dbcontext.Users.AnyAsync(user => user.Id == userId);

            if (!userExist)
            {
                throw new ResourceNotExistException($"There is no user with id: {userId}");
            }
        }

        public async Task<ICollection<FormDTO>> GetUserForms(int id)
        {
            var userForms =  await dbcontext.Forms.Where(form => form.UserId == id).ToListAsync();

            var userFormsDTO = mapper.Map<ICollection<FormDTO>>(userForms);

            return  SetQuestionsCount(userFormsDTO);
        }

        private ICollection<FormDTO> SetQuestionsCount(ICollection<FormDTO> formDTOs) 
        {
            
            foreach (var item in formDTOs)
            {

                item.QuestionsCount = item.Questions.Count();
                            
            }
            return formDTOs;
        }
        public Task<bool> SendMail(string[] emails, Guid formId)
        {
            foreach (var item in emails)
            {
                 try
                {
                    var message = new MimeMessage();
                    message.From.Add(new MailboxAddress("NegometrixAlert", "testaccnegometrix@gmail.com"));
                    message.To.Add(new MailboxAddress("Negometrix", item));
                    message.Subject = "My First Email";
                    message.Body = new TextPart("plain")
                    {
                        Text = $"https://negometrix.azurewebsites.net/AnswerForm/Index/{formId}"
                    };
                    using (var client = new SmtpClient())
                    {

                        client.Connect("smtp.gmail.com", 587, false);

                        client.Authenticate("testaccnegometrix@gmail.com", "Test11Negometrix");

                        client.Send(message);

                        client.Disconnect(true);
                    }
                }

                catch (Exception)
                {

                    return Task.FromResult(false);
                }
            }
            return Task.FromResult(true);
        }

        public void DeleteForm(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}
