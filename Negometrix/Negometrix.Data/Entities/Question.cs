﻿using Negometrix.Data.Entities.Contracts.QuestionContracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Negometrix.Data.Entities
{
    public class Question : IQuestion
    {
        public int QuestionId { get; set; }
        public Guid FormId { get; set; }
        public Form Form { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public DateTime? DeletedOn { get; set; }
        
        public bool IsDeleted { get; set; }
        public bool IsRequired { get; set; }
        public bool IsMultiple { get; set; }
        public bool IsLong { get; set; } 
        public int MaxSize { get; set; }
        public int NumberOfFiles { get; set; }       
        public string[] Options { get; set; }

        public int QuestionTypeId { get; set; }
        public QuestionType QuestionType { get; set; }
        
    }
}
