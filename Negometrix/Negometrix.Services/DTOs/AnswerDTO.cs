﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Negometrix.Services.DTOs
{
    public class AnswerDTO
    {
        public string TextResult { get; set; }
        public string[] OptionsResult { get; set; }
        public string[] FileLinksResult { get; set; }
        public int AnswerId { get; set; }
        public Guid AnswerFormId { get; set; }
        public QuestionDTO Question { get; set; }
    }
}
