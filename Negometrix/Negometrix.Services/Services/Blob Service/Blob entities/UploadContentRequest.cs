﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Negometrix.Services.Services.Blob_Service.Blob_entities
{
    public class UploadContentRequest
    {
        public string Content { get; set; }
        public string FileName { get; set; }
    }
}
