﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Negometrix.Data.Entities;
using Negometrix.Data.Seeder;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Negometrix.Data
{
    public class NegometrixContext : IdentityDbContext<User,Role,int>
    {
        public NegometrixContext(DbContextOptions<NegometrixContext> options) 
            :  base(options)
        {
        }

        public DbSet<Form> Forms { get; set; }
        public DbSet<AnswerForm> AnsweredForms { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<QuestionType> QuestionTypes { get; set; }
        public DbSet<Answer> Answers { get; set; }
        
        protected override void OnModelCreating(ModelBuilder builder)
        {
            var typesToRegister = Assembly.GetExecutingAssembly().GetTypes()
                .Where(t => t.GetInterfaces()
                    .Any(gi => gi.IsGenericType && gi.GetGenericTypeDefinition() == typeof(IEntityTypeConfiguration<>)));                  

             foreach (var type in typesToRegister) 
             { 
                 dynamic configurationInstance = Activator.CreateInstance(type);  

                 builder.ApplyConfiguration(configurationInstance); 
             }
            builder.Seeder();
            builder.RelationshipSetter();

            builder.Entity<Question>()
            .Property(e => e.Options)
            .HasConversion(
                v => string.Join(',', v),
                v => v.Split(',', StringSplitOptions.RemoveEmptyEntries));

            builder.Entity<Answer>()
            .Property(e => e.OptionsResult)
            .HasConversion(
                v => string.Join(',', v),
                v => v.Split(',', StringSplitOptions.RemoveEmptyEntries));

            builder.Entity<Answer>()
            .Property(e => e.FileLinksResult)
            .HasConversion(
                v => string.Join(',', v),
                v => v.Split(',', StringSplitOptions.RemoveEmptyEntries));


            base.OnModelCreating(builder);
        }
    }
}
