﻿using FluentValidation;
using Negometrix.Web.Models;
using Org.BouncyCastle.Math.EC.Rfc7748;
using System.Linq;

namespace Negometrix.Web.Validators
{
    public class CreateFormValidator:AbstractValidator<CreateFormViewModel>
    {
        public CreateFormValidator()
        {
            RuleFor(p => p.FormName).NotEmpty().MinimumLength(5)
                                                .MaximumLength(50);

            RuleFor(p => p.FormDescription).MaximumLength(50);

            RuleForEach(x => x.Questions).SetValidator(new CreateQuestionViewModelValidator());

            RuleFor(x => x.Questions).Must(list =>list.Count>0)
                .WithMessage("You have to create at least 1 Question in order to compleate the form");
        }
    }
}
