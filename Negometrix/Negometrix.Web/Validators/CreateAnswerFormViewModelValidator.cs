﻿using FluentValidation;
using Negometrix.Web.Models.AnswerForms;
using Negometrix.Web.Models.Anwers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Negometrix.Web.Validators
{
    public class CreateAnswerFormViewModelValidator:AbstractValidator<CreateAnswerFormViewModel>
    {
        public CreateAnswerFormViewModelValidator()
        {
            RuleForEach(x => x.Answers).SetValidator(new CreateAnswerViewModelValidator());
        }
    }
}
