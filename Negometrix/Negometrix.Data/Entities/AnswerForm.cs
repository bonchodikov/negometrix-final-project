﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Negometrix.Data.Entities
{
    public class AnswerForm
    {       
        public Guid AnswerFormId { get; set; }
        public ICollection<Answer> Answers { get; set; }
        public Guid FormId { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
