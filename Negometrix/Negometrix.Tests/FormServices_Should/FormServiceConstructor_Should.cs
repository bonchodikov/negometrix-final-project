﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Negometrix.Data;
using Negometrix.Services;
using Negometrix.Services.Contracts;
using Negometrix.Services.DTOMappers;
using Negometrix.Services.Services;
using System;
using System.Collections.Generic;

namespace Negometrix.Tests.FormServices_Should
{
    [TestClass]
   public class FormServiceConstructor_Should : BaseTest
    {
        [TestMethod]
        public void CreateInstance()
        {
            // Arrange
            var options = Utility.GetOptions(nameof(CreateInstance));

            using (var arrangeContext = new NegometrixContext(options))
            {
                // Act 
                var sut = new FormService(mapper, arrangeContext, dateTimeProvider);

                // Assert
                Assert.IsNotNull(sut);
            }
        }
        [TestMethod]
        [DynamicData(nameof(GetArguments), DynamicDataSourceType.Method)]
        public void ThrowArgumentNullException_When_ArgumentIsNull(IMapper mapper, NegometrixContext context, IDateTimeProvider dateTimeProvider)
        {
            var options = Utility.GetOptions(nameof(ThrowArgumentNullException_When_ArgumentIsNull));

            using (var arrangeContext = new NegometrixContext(options))
            {
                // Act & Assert
                Assert.ThrowsException<ArgumentNullException>(
                    () => new FormService(mapper, context, dateTimeProvider)
                );
            }
        }
        public static IEnumerable<object[]> GetArguments()
        {
            var options = Utility.GetOptions(nameof(ThrowArgumentNullException_When_ArgumentIsNull));
            object context = new NegometrixContext(options);

            IMapper mapper;
            var config = new MapperConfiguration(opts => opts.AddProfile(new DTOAutoMapper()));
            mapper = config.CreateMapper();

            IDateTimeProvider dateTimeProvider = new DateTimeProvider();

            yield return new object[] { null, context, dateTimeProvider };
            yield return new object[] { mapper, null, dateTimeProvider };
            yield return new object[] { mapper, context, null };
        }
    }
}
