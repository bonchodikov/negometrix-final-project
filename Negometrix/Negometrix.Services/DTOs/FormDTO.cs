﻿using Negometrix.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Negometrix.Services.DTOs
{
    public class FormDTO
    {
        public Guid FormId { get; set; }
        public string FormName { get; set; }
        public string FormDescription { get; set; }

        public int UserId { get; set; }
        public UserDTO User { get; set; }
        public ICollection<QuestionDTO> Questions { get; set; }

        public int QuestionsCount { get; set; }
        

        public DateTime CreatedOn { get; set; }
        public DateTime? DeletedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }
}
