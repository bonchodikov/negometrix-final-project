﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Negometrix.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Negometrix.Data.ConfigurationModels
{
    public class AnsweredFormConfig : IEntityTypeConfiguration<AnswerForm>
    {
        public void Configure(EntityTypeBuilder<AnswerForm> builder)
        {           
            builder.HasKey(p => p.AnswerFormId);
            builder.HasMany(p => p.Answers);
        }
    }
}
