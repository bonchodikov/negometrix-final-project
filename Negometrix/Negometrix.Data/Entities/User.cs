﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Negometrix.Data.Entities
{
    public class User : IdentityUser<int>
    {        
        public ICollection<Form> Forms { get; set; }
        public ICollection<AnswerForm> AnsweredForms { get; set; }
    }
}
