﻿using Negometrix.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Negometrix.Services.Contracts
{
    public interface IAnswerFormService
    {
        Task<ICollection<AnswerFormDTO>> GetAnswerForms(Guid id);
        Task<AnswerFormDTO> GetAnswerForm(Guid id);
        Task<AnswerFormDTO> CreateAnswerForm(AnswerFormDTO formDTO);
    }
}
