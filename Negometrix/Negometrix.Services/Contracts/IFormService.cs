﻿using Negometrix.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Negometrix.Services.Contracts
{
    public interface IFormService
    {
        Task<FormDTO> GetForm(Guid id);
        FormDTO GetFormNoAsync(Guid id);
        Task<ICollection<FormDTO>> GetUserForms(int id);
        Task<FormDTO> CreateForm(FormDTO formDTO);
        void DeleteForm(Guid id);
        Task<bool> SendMail(string[] emails, Guid formId);
    }
}
