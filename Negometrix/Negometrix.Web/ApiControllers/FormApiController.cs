﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Negometrix.Services.Contracts;
using Negometrix.Services.DTOs;
using Negometrix.Services.Exceptions;
using Negometrix.Web.Models;

namespace Negometrix.Web.ApiControllers
{
    [Route("api/forms")]
    [ApiController]
    public class FormApiController : ControllerBase
    {
        private readonly IMapper mapper;
        private readonly IFormService formService;

        public FormApiController(IMapper mapper,
                                 IFormService formService)
        {
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            this.formService = formService ?? throw new ArgumentNullException(nameof(formService));
        }

        [HttpGet]
        [Route("{id}")]
        public IActionResult GetForm(Guid id)
        {
            var formDTO = formService.GetForm(id);

            if (formDTO == null)
            {
                return NotFound();
            }
            else
            {
                var formViewModel = mapper.Map<ListFormViewModel>(formDTO);
                return Ok(formViewModel);
            }
        }

        [HttpPost]
        [Route("")]
        public IActionResult CreateForm([FromBody] FormViewModel formViewModel)
        {
            try
            {
                var formDTO = formService.CreateForm(mapper.Map<FormDTO>(formViewModel));

                if (formDTO == null)
                {
                    return NotFound();
                }
                else
                {
                    var formModel = mapper.Map<FormViewModel>(formDTO);
                    return Ok(formModel);
                }
            }
            catch (InvalidParameterException e)
            {

                return BadRequest(e.Message);
            }
        }
    }
}