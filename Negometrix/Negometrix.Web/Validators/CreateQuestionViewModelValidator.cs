﻿using FluentValidation;
using Negometrix.Web.Models.Questions;

namespace Negometrix.Web.Validators
{
    public class CreateQuestionViewModelValidator : AbstractValidator<CreateQuestionViewModel>
    {
        public CreateQuestionViewModelValidator()
        {
            RuleFor(p => p.Name).NotEmpty().MinimumLength(5)
                .MaximumLength(50)
                .WithMessage("Minimum characters for Question Title : 5 symbols and maximum lenght of 50 symbols");

            RuleFor(p => p.Description).MaximumLength(50);

            RuleFor(p => p.QuestionType.QuestionTypeName).NotEqual("none").WithMessage("Please select question type");

            RuleForEach(x => x.Options).NotNull().MaximumLength(20);

            RuleFor(x => x.Options).Must(list => list.Count >= 2).When(p => p.QuestionType.QuestionTypeName.Equals("option"))
                .WithMessage("You should have at least two options in order to create an option question");
        }
    }
}
