﻿using Microsoft.AspNetCore.Http;
using Negometrix.Web.Models.Anwers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Negometrix.Web.Models.AnswerForms
{
    public class CreateAnswerFormViewModel
    {
        public CreateAnswerFormViewModel()
        {
            this.Answers = new List<CreateAnswerViewModel>();
 
        }

        public string AnswerFormName { get; set; }
        public string AnswerFormDescription { get; set; }
        public Guid FormId { get; set; }
        public List<CreateAnswerViewModel> Answers { get;set; }
    }

}
