﻿using Negometrix.Data.Entities.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Negometrix.Data.Entities
{
    public class Form : IEntities
    {       
        public Guid FormId { get; set; }
        public string FormName { get; set; }
        public string FormDescription { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }
        public int QuestionsCount { get; set; } = 0;
        public DateTime CreatedOn { get ; set; }
        public DateTime? DeletedOn { get; set; }
        public DateTime? ModifiedOn { get; set ; }
        public ICollection<AnswerForm> AnsweredForms { get; set; }
        public ICollection<Question> Questions { get; set; }
        public bool IsDeleted { get; set; }
        
    }
}
