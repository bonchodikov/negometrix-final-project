﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Negometrix.Services.DTOs
{
    public class QuestionTypeDTO
    {
        public int QuestionTypeId { get; set; }
        public string QuestionTypeName { get; set; }
    }
}
