﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Negometrix.Data;
using Negometrix.Services.Contracts;
using Negometrix.Services.DTOMappers;
using System;

namespace Negometrix.Tests
{
    public abstract class BaseTest
    {
        protected IMapper mapper;
        protected IDateTimeProvider dateTimeProvider;

        public BaseTest()
        {
            var config = new MapperConfiguration(opts => opts.AddProfile(new DTOAutoMapper()));
            mapper = config.CreateMapper();

            var mock = new Mock<IDateTimeProvider>();
            mock.Setup(x => x.GetDateTime())
                .Returns(Convert.ToDateTime("06-12-2020"));

            dateTimeProvider = mock.Object;
        }

        [TestCleanup()]
        public void Cleanup()
        {
            var options = Utility.GetOptions(nameof(TestContext.TestName));

            var context = new NegometrixContext(options);
            context.Database.EnsureDeleted();
        }
    }
}
