﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Negometrix.Services.DTOs
{
    public class AnswerFormDTO
    {
        public Guid AnswerFormId { get; set; }
        public Guid FormId { get; set; }
        public string AnswerFormName { get; set; }
        public string AnswerFormDescription { get; set; }
        public ICollection<AnswerDTO> Answers { get; set; }
    }
}
