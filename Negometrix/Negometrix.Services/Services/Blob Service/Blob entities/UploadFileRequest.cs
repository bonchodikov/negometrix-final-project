﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Negometrix.Services.Services.Blob_Service.Blob_entities
{
    public class UploadFileRequest
    {
        public string FilePath { get; set; }
        public string FileName { get; set; }
    }
}
